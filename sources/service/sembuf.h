#ifndef SEMBUF_H
#define SEMBUF_H

#include <sys/sem.h>

static struct sembuf sem_lock = { 0, -1, IPC_NOWAIT };
static struct sembuf sem_unlock = { 0, 1, IPC_NOWAIT };

/* Мы должны определить  union semun  самостоятельно.  */ 
	union semun {
		int val;
		struct semid_ds *buf;
		unsigned short int *array;
		struct seminfo *__buf;
	};
	/* Получить идентификатор двоичного семафора, выделяя его в случае  необходимости. */
        inline int binary_semaphore_allocation (key_t key, int sem_flags)
	{
		return semget (key, 1, sem_flags);
	}
	/* Освобождаем двоичной семафор. Все пользователи должны закончить использование семафора 
		к этому моменту.  Возвращает -1 при ошибке. */ 
        inline int binary_semaphore_deallocate (int semid)
	{
		union semun ignored_argument;
		return semctl (semid, 1, IPC_RMID, ignored_argument);
	}

	/* обслужить двойной  семафор. блокировка, пока   значение  семафора  положительное, заетм  декремент на 1. */ 
        inline int binary_semaphore_wait (int semid)
	{
		struct sembuf operations[1];
		/* использовать первый  (и только) семафор. */ 
		operations[0].sem_num = 0;
		/*  декремент 1. */
		operations[0].sem_op = -1;
		/*  разрешается отмена операции*/
		operations[0].sem_flg = SEM_UNDO;
		return semop (semid, operations, 1);
	}
	/* пост к двоичному  семафору: увеличиваем его значение на 1. это выполнится немедленно. */
        inline int binary_semaphore_post (int semid)
	{
		struct sembuf operations[1];
		/* используем  первый семафор. */
		operations[0].sem_num = 0;
		/* increment by 1. */
		/* приращение на 1. */
		operations[0].sem_op = 1;
		/*разрешаем отмену операции. */ 
		operations[0].sem_flg = SEM_UNDO;
		return semop (semid, operations, 1);
	}


inline int binary_semaphore_unlock (int semid)
{
	if( semop(semid, &sem_unlock, 1) < 0) 
	{
//		printf("ошибка разблокировки семафора\n");
		return -1;
	}
	else
		return 0;
}



inline int binary_semaphore_lock (int semid)
{
int i;

	if( semop(semid, &sem_lock, 1) < 0)
	{
//		printf("ошибка блокировки семафора\n");
		return -1;
	}
	else
		return 0;
}

#endif // SEMBUF_H
