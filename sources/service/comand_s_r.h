#ifndef COMAND_S_R_H
#define COMAND_S_R_H

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <string.h>

#define MSGSZ     10
#define IN_SERVICE_KEY 11
#define OUT_SERVICE_KEY 12

/* команды */
#define STOP 1//завершить измерение и остановить сервис (отключить прибор)
#define ECHO 2//эхо: отправить ответ 2
#define PROTOCOL_VERSION 3//получить версию протокола
#define STATUS 4 //определить статус выполнения программы
#define STATUS_CONNECT 401     // ответ на команду STATUS. подключение прибора
#define STATUS_MEASUREMENT 402 // ответ на команду STATUS. измерение
/***********/
/**/
typedef struct  {
         long    mtype;//тип сообщения. всегда равен 1
	 int cmd;//номер команды
//         char    mtext[MSGSZ];//параметры
         } msgBuf;
/**/

inline int init(key_t key)
{
    int msqid;
    int msgflg = IPC_CREAT ;

//    

//    printf("Calling msgget with key %#lx and flag %#o\n",key,msgflg);

    if ((msqid = msgget(key, msgflg )) < 0) {
        perror("msgget");
        return -1;
    }
//    else 
//     printf("msgget: msgget succeeded: msqid = %d\n", msqid);
return msqid;
}


inline int send_msg(int msqid, msgBuf *sbuf)
{/**
    size_t buf_length;
    buf_length = strlen(sbuf->mtext) + 1 ;
    /**/
    if (msgsnd(msqid, sbuf, sizeof(msgBuf), IPC_NOWAIT) < 0) {
//       printf ("\nошибка отправки msgsnd %d, %d,  %d\n\n", msqid, sbuf->mtype, sbuf->cmd);
//        perror("msgsnd");
        return -1;
    }

      
    return 0;
}
/*-------------------------------------------*/
/*-------------------------------------------*/


inline int recv_msg(int msqid,msgBuf *rbuf)
{    
    return msgrcv(msqid, rbuf, sizeof(msgBuf), 1,  IPC_NOWAIT);
}
/*-------------------------------------------*/

#endif // COMAND_S_R_H

