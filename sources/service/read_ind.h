#ifndef READ_IND_H
#define READ_IND_H

#include <stdio.h>
#include "sembuf.h"
#include "buferXYZ.h"




    static key_t key_ind_buf = SEMINDID;
    static int sem_ind_buf;

inline int init_buf(){
    if( ( sem_ind_buf = binary_semaphore_allocation(key_ind_buf,IPC_CREAT) ) < 0)
    {
//       printf("ошибка получения индификатора\n");
       return -1;
    }
    else
    {
//       printf("%s%d%c","получен индификатор семафора ",sem_ind_buf,'\n');
    }
    if( binary_semaphore_post (sem_ind_buf) < 0) 
    {
//       printf("ошибка увеличения значения семафора\n");
    }
    else return 0;
}


inline int read_ind(BuferData *dat) // ошибки: -1 блокировка симафора ; -2 открытие временного файла ; -3 чтение из ввременного файла ; -4 разблокировка симафора
{
int i;
int err = 0;
	if(binary_semaphore_lock (sem_ind_buf) < 0) err = -1;
	FILE *bufer_ind;
	bufer_ind = fopen(BUFER_IND, "rb");
	if(bufer_ind<=0) err = -2;
	if ( fread(dat,1,sizeof(BuferData),bufer_ind) < 0) err = -3;
	fclose(bufer_ind);
	if(binary_semaphore_unlock ( sem_ind_buf ) < 0) err = -4;
return err;
}

#endif // READ_IND_H




