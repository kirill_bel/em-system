#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <glob.h>
#include <stdlib.h>

#define time_get_fra_0 1000000
#define time_get_fra_1 500000


int get_fra (int fd, unsigned char*  M)
{//начало функции 
int L; //длина поля данных
int ind=-1; //возвращаемое значение
int r;//счётчик байтов
unsigned char t;//переменная - байт
unsigned char crc1=0x00;//первая контрольная сумма
unsigned char crc2=0x00;//вторая контрольная сумма
unsigned char sync[2];
int y=0; //счётчик "левых" байтов


  fd_set set;
  struct timeval timeout;
  int rv;
 
 
  FD_ZERO(&set); /* clear the set */
  FD_SET(fd, &set); /* add our file descriptor to the set */

  timeout.tv_sec = 0;
  timeout.tv_usec = time_get_fra_0;

  rv = select(fd + 1, &set, NULL, NULL, &timeout);


  if (rv==0){ind = -2;}

else{//1  //начало else от тайм-аутов

time_start();
while(1)
{//от while
if(select(fd + 1, &set, NULL, NULL, &timeout)==0){ind=-2; break;}
read(fd, &sync, 2);//байты синхронизации
//printf("time %d\n",time_stop()); 
if(time_stop()>time_get_fra_1){ind=-3; break;}


if (sync[0]==0x80&sync[1]==0xfe)
{//3
r=0;
crc1=0x80;
M[r]=0x80;
r++;
crc1=crc1^0xfe;
M[r]=0xfe;
r++;
if(select(fd + 1, &set, NULL, NULL, &timeout)==0){ind=-2; break;}
read(fd, &sync, 2);//размер поля данных
if(time_stop()>time_get_fra_1){ind=-3; break;}
crc1=crc1^sync[0];
M[r]=sync[0];
r++;
crc1=crc1^sync[1];
M[r]=sync[1];
r++;
L = (sync[0]<<8)|sync[1];
if(select(fd + 1, &set, NULL, NULL, &timeout)==0){ind=-2; break;}
read(fd, &t, 1);//поле контрольной суммы заголовка
if(time_stop()>time_get_fra_1){ind=-3; break;}

if (crc1==t)
{
M[r]=t;
r++;
crc2=crc1;
/**/
while (r<5+L)
{
if(select(fd + 1, &set, NULL, NULL, &timeout)==0){ind=-2; break;}
read(fd, &t, 1);//байт данных
if(time_stop()>time_get_fra_1){ind=-3; break;}
M[r]=t;
crc2=crc2^t;
r++;
}

/**/
if(select(fd + 1, &set, NULL, NULL, &timeout)==0){ind=-2; break;}
read(fd, &t, 1);//поле контрольной суммы данных
if(time_stop()>time_get_fra_1){ind=-3; break;}
if (crc2==t)
{M[r]=t;
ind = r;}
else
{ind=-1;}
break;
}//от первой контрольной суммы
//else {ind = -1; break;}
//if(time_stop()>100000){ind=-3; break;}
}//от if syncro


else {y++;  if(y>1000){ind=-3; break;}}//3
}//от while


}//от timeout


return ind;
}// от самой функции






int open_port() //корректное открытие порта
{
  int fdo=-1; /* Файловый дескриптор для порта */
  int fd;


int n=-1;
unsigned char  M[500]={0};
size_t i=0;
unsigned char  reset[]={0x80, 0xfe, 0x00, 0x01, 0x7f, 0x71, 0x0e};//сброс
unsigned char  start[] = {0x80, 0xfe, 0x00, 0x01, 0x7f, 0x32, 0x4d};//запуск измерения
unsigned char  set_speed[]={0x80, 0xfe, 0x00, 0x01, 0x7f, 0x57, 0x28};//установка скорости передачи данных
unsigned char  stop[] = {0x80, 0xfe, 0x00, 0x01, 0x7f, 0x33, 0x4c};//остановка измерительного процесса
int status; //переменная для перевода в 485-й интерфейс


    glob_t globbuf;
    int err = glob("/dev/ttyM*", 0, NULL, &globbuf);
    //printf("длина массива портов:%d\n",globbuf.gl_pathc);
    if(err == 0)
    { 
        for ( i=0;i < globbuf.gl_pathc&n<0; i++)
        {








fd = open(globbuf.gl_pathv[i], O_RDWR | O_NOCTTY | O_NONBLOCK);


/*      char* str1;
      char* str2;
      str1 = "setserial ";
      str2 = globbuf.gl_pathv[i];
      char * str3 = (char *) malloc(1 + strlen(str1)+ strlen(str2) );
      strcpy(str3, str1);
      strcat(str3, str2);
      char* str4;
      str4=" port 3";
      char * str5 = (char *) malloc(1 + strlen(str3)+ strlen(str4) );
      strcpy(str5, str3);
      strcat(str5, str4);

status=system(str5);*/


//status=system(strcat(strcat("setserial ",globbuf.gl_pathv[i])," port 3"));
//printf("RS-485: %d\n",status);
  //printf("номер порта: %d\n",fd);


/**/
  //printf("as: %d\n",as);
  if (fd == -1)
  {
   /*
    * Could not open the port.
    */

    //perror(strcat("Невозможно открыть порт ",globbuf.gl_pathv[i] ));
  }
  else
    fcntl(fd, F_SETFL, 0);
     


  //printf("номер порта: %d\n",fd);

/* *** Configure Port *** */
  struct termios options;

  tcgetattr(fd, &options); 
  //setting baud rates and stuff
  cfsetispeed(&options, B230400);
  cfsetospeed(&options, B230400);
  //options.c_cflag |= (CLOCAL | CREAD);
  tcsetattr(fd, TCSANOW, &options);

  tcsetattr(fd, TCSAFLUSH, &options);



 /* options.c_cflag &= ~PARENB;//next 4 lines setting 8N1
  options.c_cflag &= ~CSTOPB;
  options.c_cflag &= ~CSIZE;
  options.c_cflag |= CS8;

  //options.c_cflag &= ~CNEW_RTSCTS;

  options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); //raw input

  options.c_iflag &= ~(IXON | IXOFF | IXANY); //disable software flow control*/

//tcflush(fd,TCIOFLUSH);
   write(fd,stop, sizeof(stop));
   write(fd,reset, sizeof(reset));
    sleep(1);  
    n=get_fra (fd,M);
   // printf("%d\n",n);
    if (n>0){
//    printf("Общий сброс прибора выполнен.\n");
    fdo=fd;


while(1){
write(fd,start, sizeof(start));//запуск измерения
    n=get_fra (fd,M);
    if (n>0){
//    printf("Измерение запущено.\n");
    break;
} 
}



/**/



}












            //printf("%s\n", globbuf.gl_pathv[i]);
        }
       // printf("99\n");
        globfree(&globbuf);
    }



  return (fdo);
}





void cor_close_port(int fd){//корректное закрытие порта

unsigned char  reset[] = {0x80, 0xfe, 0x00, 0x01, 0x7f, 0x71, 0x0e};//сброс прибора
unsigned char  stop[] = {0x80, 0xfe, 0x00, 0x01, 0x7f, 0x33, 0x4c};//остановка измерительного процесса
int n=-1;
unsigned char  M[500]={0};

write(fd,stop, sizeof(stop));//остановка измерения
//    printf("Остановка измерительного процесса\n");

sleep(1);

write(fd,reset, sizeof(reset));

//    printf("Общий сброс прибора\n");



close(fd); //закрытие порта


}


















