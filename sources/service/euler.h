

#include "../../dependencies/mpfit.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


#define NG 6 //число катушек
#define NS 8 //число каналов

#define NB 3*NG //число значений индукции
#define NSA 6*NS //число переменных для описания каналов
#define NBA NB*NS //размерность матрицы индукции


struct vars_struct {
  double *B;
};

double dip_b(const double nx,const double ny,const  double nz,const double Nx,const double Ny,const  double Nz,const double Rx,const  double Ry,const  double Rz)//расчёт поля диполя
{
double R2=0;
R2=sqrt(Rx*Rx+Ry*Ry+Rz*Rz);
return (3*(Rx*Nx+Ry*Ny+Rz*Nz)*(nx*Rx+ny*Ry+nz*Rz)-R2*R2*(nx*Nx+ny*Ny+nz*Nz))/(R2*R2*R2*R2*R2);

}






int gaussfunc(int m, int n, double *p, double *dy, double **dvec, void *vars)
{

  struct vars_struct *v = (struct vars_struct *) vars;
  double* B;
  B = v->B;


double X[NG]={-65  , -39  ,39    ,0      ,65   ,0};//массив абсцисс координат источников
double Y[NG]={0  , 60.4  ,60.4  ,112.8   ,0   ,7.5};//массив ординат координат источников
double Z[NG]={0  , 0   ,0 ,0   ,0   ,0};//массив аппликат координат источников

const double as=(90-64)*M_PI/180;
const double ap=60*M_PI/180;



const double Nx[NG]={sin(as)*cos(M_PI/6)  , -cos(ap)  ,-cos(ap)   ,0             ,-sin(as)*cos(M_PI/6)   
,1};//массив абсцисс направлений источников
const double Ny[NG]={sin(as)*sin(M_PI/6)  , -sin(ap)  ,sin(ap)  ,-sin(as)  ,sin(as)*sin(M_PI/6)   
,0};//массив ординат направлений источников
const double Nz[NG]={cos(as)             , 0           ,0       ,cos(as)  ,cos(as)   
,0};//массив аппликат направлений источников





//магнитные моменты источников
const double Md[NG]={1900000000,1912000000,1856000000,1940000000,1916000000,1900000000,};


//направления трёх датчиков
double p1x,p1y,p1z;
double p2x,p2y,p2z;
double p3x,p3y,p3z;

//компоненты векторов "датчик-источник"
double rx,ry,rz;
int i;
const double lzy=4; //смещение между Z и Y
const double lzx=8; //смещение между Z и X



//вычисление всех направлений у трех датчиков  канала j
p1x=cos(p[3])*cos(p[4]);
p1y=cos(p[3])*sin(p[4]); //первый датчик
p1z=-sin(p[3]);

p2x=cos(p[4])*sin(p[3])*sin(p[5])-cos(p[5])*sin(p[4]);
p2y=cos(p[5])*cos(p[4])+sin(p[3])*sin(p[5])*sin(p[4]); //второй датчик
p2z=cos(p[3])*sin(p[5]);

p3x=-(p1y*p2z-p2y*p1z);
p3y=-(-p1x*p2z+p2x*p1z);    //третий датчик
p3z=-(p1x*p2y-p2x*p1y);


for (i=0;i<NG;i++){

rx=p[0]-X[i];
ry=p[1]-Y[i]; //радиус-вектор источника i
rz=p[2]-Z[i];

(dy)[3*i+0]=Md[i]*dip_b(p1x,p1y,p1z,Nx[i],Ny[i],Nz[i],rx-p3x*lzx,ry-p3y*lzx,rz-p3z*lzx)-B[3*i+0];//Сигнал с датчика 1 от источника i


(dy)[3*i+1]=Md[i]*dip_b(p2x,p2y,p2z,Nx[i],Ny[i],Nz[i],rx-p3x*lzy,ry-p3y*lzy,rz-p3z*lzy)-B[3*i+1];//Сигнал с датчика 2 от источника i

  
(dy)[3*i+2]=Md[i]*dip_b(p3x,p3y,p3z,Nx[i],Ny[i],Nz[i],rx,ry,rz)-B[3*i+2];//Сигнал с датчика 3 от источника i



}


  return 0;
}




int testgaussfix(double* p,double* B)
{
//int j; for(j=0;j<18;j++) printf("B[%d] = %.0f  ",j,B[j]); printf("\0");
//for(j=0;j<6;j++) printf("p[%d] = %.0f  ",j,p[j]); printf("\0");

  double perror[6];			   /* Returned parameter errors */     
  mp_par pars[6];			   /* Parameter constraints */         
  int i;
  struct vars_struct v;
  int status;
  mp_result result;

  memset(&result,0,sizeof(result));  /* Zero results structure */
  result.xerror = perror;

 memset(pars,0,sizeof(pars));    // Initialize constraint structure */
  //pars[0].fixed = 1;              // Fix parameters 0 and 2 */
  //pars[2].fixed = 1;

  /*How to put limits on a parameter.  In this case, parameter 3 is
     limited to be between -0.3 and +0.2.*/
 /* pars[0].limited[0] = 0;    
  pars[0].limited[1] = 1;
  pars[0].limits[0] = -30;
  pars[0].limits[1] = +30;
  pars[1].limited[0] = 0;    
  pars[1].limited[1] = 1;
  pars[1].limits[0] = -30;
  pars[1].limits[1] = +30; 
  pars[2].limited[0] = 0;    
  pars[2].limited[1] = 1;
  pars[2].limits[0] = 200-30;
  pars[2].limits[1] = 200+30;*/


  v.B = B;

  /* Call fitting function for 10 data points and 4 parameters (2
     parameters fixed) */


  status = mpfit(gaussfunc, NB, 6, p, pars, 0, (void *) &v, &result);
//printf("label2\n");
  return 0;
}


