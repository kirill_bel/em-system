/*  версия для демонстрационо-отладочного эмулятора сервиса хирургической станции */

#include <stdio.h>   /* Стандартные объявления ввода/вывода */
#include <string.h>  /* Объявления строковых функций */
#include <unistd.h>  /* Объявления стандартных функций UNIX */
#include <fcntl.h>   /* Объявления управления файлами */
#include <errno.h>   /* Объявления кодов ошибок */
#include <termios.h> /* Объявления управления POSIX-терминалом */
//#include "generate_packs.h"//генерирование пакетов, соотв. определ. скорости
#include "interp.h"//интерпретация пакетов
//#include "coordinates_a.h"//функции, относящиеся к определению координат
//#include "euler.h"
#include "../../dependencies/mpfit.h"
#include <stdlib.h>
#include <math.h>
#include "buferXYZ.h"
#include "sembuf.h"
#include <sys/time.h>
#include "timer.h"
#include "get_pack.h"//запись пакета в массив
#include <glob.h>
#include "comand_s_r.h"
/*
int get_fra(int,unsigned char*)  ;//принятие пакета
int open_port();      //Функция открытия порта
void get_ind(unsigned char*  M,double* p1,double* p2,double* p3,double* p4,double* p5,double* p6,double* p7,double* p8,unsigned char *byte_channels,int *channels); // получить значения индукции и координат
int testgaussfix(double* ,double* );    //пересчитать индукцию в координаты
void cor_close_port(int); //корректно закрыть порт
/**/
int measurement();
void measurement_end();
void device_close();
int initServ();

BuferData dat;//структура для записи в буфер
int flag_end=0;
int next=0;


double p1[]={0,150,0,0,0,0}; 
double p2[]={0,150,0,0,0,0}; 
double p3[]={0,150,0,0,0,0}; 
double p4[]={0,150,0,0,0,0}; //массивы координат и углов
double p5[]={0,150,0,0,0,0}; 
double p6[]={0,150,0,0,0,0}; 
double p7[]={0,150,0,0,0,0}; 
double p8[]={0,150,0,0,0,0}; 

/*
double B1[NB]={0};
double B2[NB]={0};
double B3[NB]={0};
double B4[NB]={0};   //массивы индукции
double B5[NB]={0};
double B6[NB]={0};
double B7[NB]={0};
double B8[NB]={0};
/**/

unsigned char byte_channels[CHENNELS];
int channels[CHENNELS];//информация о подключенных каналах: 0 - датчик подключен, измерение в норме; -1 - датчик отключен; -2 - зашкал; -3 - датчик далеко от генератора


int i;
int n;
int fd; // открытие порта

key_t key;
int sem;
/**    для управления сервисом    */
    key_t ikey;
    msgBuf rbuf, sbuf;
    int imsqid;
    key_t okey;
    int omsqid;
/**********************************/
char *r;
unsigned char  byte;
unsigned char  M[500]={0};
unsigned char  reset[] = {0x80, 0xfe, 0x01, 0x7f, 0x71, 0x71};//сброс прибора
unsigned char  start[] = {0x80, 0xfe, 0x01, 0x7f, 0x32, 0x32};//запуск измерения
unsigned char  stop[] = {0x80, 0xfe, 0x01, 0x7f, 0x33, 0x33};//остановка измерительного процесса


int measurement()
{
    dat.error_flag = 0;
    n=get_fra (fd,M);/**/
    if(n<=0)  // обрабатываем ошибку связи
    {
	dat.error_flag = n;
//	switch(n){case -3:printf("Проверьте связь с USB-адаптером!\n");break;	case -2:printf("Проверьте питание прибора!\n");break;	}
//	return n;
    }
    else
    {
	dat.error_flag = get_ind(M,p1,p2,p3,p4,p5,p6,p7,p8,channels,byte_channels);//,byte_channels,channels);//записать в переменные B1 и B2 индукции с 1-го и 2-го канала

/*  проверяем буфер команд  */
      if(recv_msg(imsqid, &rbuf) >= 0)
      {
	switch(rbuf.cmd)
	{
		case STOP: sbuf.mtype=1; sbuf.cmd=STOP; send_msg(omsqid, &sbuf); flag_end = 1; break;
		case ECHO: sbuf.mtype=1; sbuf.cmd=ECHO; send_msg(omsqid, &sbuf); break;
		case STATUS: sbuf.mtype=1; sbuf.cmd=STATUS_MEASUREMENT; send_msg(omsqid, &sbuf); break;
//		case STOP_MEASUREMENT: send_msg(omsqid, &rbuf); flag_end = 1; break;
//		default: flag_end = 1;break;
	}
      }
/****************************/
    }


/////           создаём запись для буфера     *
    int k;
    for(k=0;k<LENGTH_OF_ARRAY;k++)
    {
	dat.p1[k]=p1[k];
	dat.p2[k]=p2[k];
	dat.p3[k]=p3[k];
	dat.p4[k]=p4[k];
	dat.p5[k]=p5[k];
	dat.p6[k]=p6[k];
	dat.p7[k]=p7[k];
	dat.p8[k]=p8[k];
	if(k == 2)
	{
	  dat.p1[k]=abs(p1[k]);
	  dat.p2[k]=abs(p2[k]);
	  dat.p3[k]=abs(p3[k]);
	  dat.p4[k]=abs(p4[k]);
	  dat.p5[k]=abs(p5[k]);
	  dat.p6[k]=abs(p6[k]);
	  dat.p7[k]=abs(p7[k]);
	  dat.p8[k]=abs(p8[k]);
	}
    }
    for(k=0;k<CHENNELS;k++)
    {
	dat.channels[k]=channels[k];
	dat.byte_channels[k]=byte_channels[k];
    }
   

struct timezone tz;
    gettimeofday(&dat.time, &tz);;
    dat.pack_number = i;

/*         запись в буфер     */
	binary_semaphore_lock (sem);
	FILE *bufer_ind;
	bufer_ind = fopen(BUFER_IND, "wb");
	fwrite(&dat,1,sizeof(BuferData),bufer_ind);
	fclose(bufer_ind);
	binary_semaphore_unlock ( sem );
/******************************/
	return dat.error_flag;
}


void measurement_end()
{
//printf(" штатное завершение\n");
/*           создаём запись для буфера о штатном завершении программы    */

    time(&dat.time);
    dat.pack_number = i;
    dat.error_flag = 1;
/*         запись в буфер     */
	binary_semaphore_lock (sem);
	FILE *bufer_ind;
	bufer_ind = fopen(BUFER_IND, "wb");
	fwrite(&dat,1,sizeof(BuferData),bufer_ind);
	fclose(bufer_ind);
	binary_semaphore_unlock ( sem );

/******************************/


/**/
write(fd,stop, sizeof(stop));//остановка измерения
//sleep(1);
    n=get_fra (fd,M);
   
//    printf("Остановка измерительного процесса %d\n", n);
    //print_pack(M);
/**/


/*          удаление семафора   */
    if( binary_semaphore_deallocate (sem) < 0 ) 
    {
//	printf("ошибка удаления семафора\n");
    }
    else
    {
//	printf("семафор удалён\n");
    }

/********************************/
}


int connect(long timeout)
{  
time_start();
fd = -1;
int end = 0;
while((fd<0) && (end == 0)  )
{
/*  проверяем буфер команд  */
if(recv_msg(imsqid, &rbuf) >= 0)
{
	switch(rbuf.cmd)
	{
		case STOP: sbuf.mtype=1; sbuf.cmd=STOP; send_msg(omsqid, &sbuf); end = 1;flag_end = 1;return -1; break;
		case ECHO: sbuf.mtype=1; sbuf.cmd=ECHO; send_msg(omsqid, &sbuf); break;
		case STATUS: sbuf.mtype=1; sbuf.cmd=STATUS_CONNECT; send_msg(omsqid, &sbuf); break;
	}
}
/****************************/
//printf("open_port start\n");
fd=open_port();
//printf("open_port returned fd = %d\n",fd);
if (time_stop() > timeout) return -2;
}
/**/  
return 0;
}

int initServ()
{/**
for(i=0;i<NS;i++)//генерация начального приближения
{
p[6*i+2]=150;
}
/**    для управления сервисом    */
    ikey = IN_SERVICE_KEY;
    imsqid = init(ikey);
    okey = OUT_SERVICE_KEY;
    omsqid = init(okey);
    while(recv_msg(imsqid, &rbuf) == 0);//очищаем очередь сообщений
/**********************************/ 


// проверяем запущенли уже сервис
while(recv_msg(imsqid, &rbuf) == 0);//очищаем буфер
while(recv_msg(omsqid, &rbuf) == 0);//очищаем буфер
sbuf.mtype=1; sbuf.cmd=ECHO; send_msg(imsqid, &sbuf);
time_start();
while(time_stop()<1000000){
  rbuf.mtype=1;
  if(recv_msg(omsqid, &rbuf) > 0) { return -9;}
}
while(recv_msg(imsqid, &rbuf) == 0);//очищаем буфер
/******************************/

/*   создаём семафор    */
    key = SEMINDID;
    if( ( sem = binary_semaphore_allocation(key,IPC_CREAT) ) < 0)
    {
//       printf("ошибка получения индификатора\n");
    }
    else
    {
//       printf("%s%d%c","получен индификатор семафора ",sem,'\n');
    }
    if( binary_semaphore_post (sem) < 0);// printf("ошибка увеличения значения семафора\n");
/*********************/

return connect(3000000);
}


