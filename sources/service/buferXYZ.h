#ifndef BUFERXYZ_H
#define BUFERXYZ_H

#include <sys/time.h>

#define BUFER_IND "/tmp/buf_xyz.buf"

#define LENGTH_OF_ARRAY 6
#define COILS 8
#define CHENNELS 8
#define SEMINDID 1492
#define DATA_TYPE double
#define TIME_TYPE struct timeval




typedef struct 
{
    DATA_TYPE p1[LENGTH_OF_ARRAY];
    DATA_TYPE p2[LENGTH_OF_ARRAY];
    DATA_TYPE p3[LENGTH_OF_ARRAY];
    DATA_TYPE p4[LENGTH_OF_ARRAY];
    DATA_TYPE p5[LENGTH_OF_ARRAY];
    DATA_TYPE p6[LENGTH_OF_ARRAY];
    DATA_TYPE p7[LENGTH_OF_ARRAY];
    DATA_TYPE p8[LENGTH_OF_ARRAY];

    unsigned char coil_channel[COILS][CHENNELS];//информация о парах катушка - датчик
    unsigned char byte_channels[CHENNELS];//информация о каналах
    int channels[CHENNELS];//информация о подключенных каналах: 0 - датчик подключен, измерение в норме; -1 - датчик отключен; -2 - зашкал; -3 - датчик далеко от генератора
    TIME_TYPE time;
    int pack_number;
    int error_flag;
} BuferData;


int getP(DATA_TYPE *pp,BuferData *dat,int channel);//получение массива первого датчика
TIME_TYPE getTime(BuferData dat);//получение времени создания пакета
int getPackNumber(BuferData dat);//получение номера пакета
int getErrorFlag(BuferData dat);//получение информации об ошибках


inline int getP(DATA_TYPE *pp,BuferData *dat,int channel)
{
	DATA_TYPE *p;
	int i;

	switch(channel)
	{
	  case 1: p = dat->p1;break;
	  case 2: p = dat->p2;break;
	  case 3: p = dat->p3;break;
	  case 4: p = dat->p4;break;
	  case 5: p = dat->p5;break;
	  case 6: p = dat->p6;break;
	  case 7: p = dat->p7;break;
	  case 8: p = dat->p8;break;
	  default: return -1;
	}
	for(i=0;i<6;i++) pp[i] = p[i];
	return 0;
}
inline TIME_TYPE getTime(BuferData dat)
{
	return dat.time;
}
inline int getPackNumber(BuferData dat)
{
	return dat.pack_number;
}
inline int getErrorFlag(BuferData dat)
{
	return dat.error_flag;
}

#endif // BUFERXYZ_H
