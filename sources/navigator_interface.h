#ifndef NAVIGATOR_INTERFACE_H
#define NAVIGATOR_INTERFACE_H

#include <cstdlib>
#include <unistd.h>
#include <string.h>
#include "./service/read_ind.h"
#include "./service/timer.h"
//#include "../navigator_v1/buferXYZ.h" // для передачи измерений оставляем старый протокол и структуру BuferData
#include "./service/comand_s_r.h"

#define EM_CHANNEL_COUNT 8
/**
	typedef struct 
	{
		int proto_id; //идентификатор протокола
		int bufsize;  //размер данных
		void* pData;  //данные пакета (различаются в зависимости от протокола). В pData каждый протокол хранит свою структуру данных пакета, с которой только он умеет работать.
	} BuferComandData; //содержит данные для конкретного протокола
/**/


class InstrumentCount;

class NavigatorInterface
{/**/
public:
//	NavigatorInterface();
//	~NavigatorInterface();
	virtual int initialize() = 0;	//Инициализация   (  вызывается конструктором, нужна ли? )
	virtual int read_data() = 0;	//Чтение в буфер 
	virtual int protocol_version() = 0;	 //Версия протокола
	
	virtual int getCoord(DATA_TYPE *Coord,int channel) = 0;
	virtual int getOrientation(DATA_TYPE *Orientation,int channel) = 0;
	virtual int getInstrumentInfo(InstrumentCount *instrument) = 0;// запрос об установленом инструменте (информация не передаётся с измерением)
	virtual int *getChennelsStatus() = 0;
	virtual TIME_TYPE getTime() = 0;
	virtual int getPackNumber() = 0;
	virtual int getErrorFlag() = 0;
	virtual int startService(char *adr_to_service) = 0;
	virtual int shutDownService() = 0;// выключить сервис
	virtual int rebootService() = 0;// перезагрузить сервис
/**/
};

//--------------------------------------------

class cProtocol_v01 //: public NavigatorInterface //Версия протокола №1
{
private:
	BuferData data;
	msgBuf rbuf;
	int imsqid;
	msgBuf sbuf;
	int omsqid;
public:	
	int initialize(); // ошибки: -1 инициализация входящего канала ; -2 инициализация исходящего канала ; -3 инициализация буфера координат
	int read_data();	 // ошибки: -1 блокировка симафора ; -2 открытие временного файла ; -3 чтение из ввременного файла ; -4 разблокировка симафора
//	int protocol_version(); // ошибки: -1 отправка пакета ; -2 получение пакета ; -3 получен неверный ответ
	int getCoord(DATA_TYPE *Coord,int channel); // возвращает -1 если пакет не содержит результатов измерений
	int getOrientation(DATA_TYPE *Orientation,int channel); // возвращает -1 если пакет не содержит результатов измерений
	int *getChennelsStatus();//информация о подключенных каналах: 0 - датчик подключен, измерение в норме; -1 - датчик отключен; -2 - зашкал; -3 - датчик далеко от генератора
	unsigned char getByteChannel(int channel);//информация о каналах
	TIME_TYPE getTime();
	int getPackNumber();
	int getErrorFlag();
	int startService(char *adr_to_service);// возвращает значение командной строки
	int shutDownService(); // ошибки: -1 отправка пакета ; -2 получение пакета ; -3 получен неверный ответ
	int echo(); // ошибки: -1 отправка пакета ; -2 получение пакета ; -3 получен неверный ответ
};

inline int cProtocol_v01 :: initialize() // ошибки: -1 инициализация входящего канала ; -2 инициализация исходящего канала ; -3 инициализация буфера координат
{
    imsqid = init(OUT_SERVICE_KEY);
    omsqid = init(IN_SERVICE_KEY);
if(imsqid < 0) return -1;
if(omsqid < 0) return -2;
if(init_buf() < 0) return -3;
rbuf.mtype = 1;
while(recv_msg(imsqid, &rbuf) >= 0);
   sbuf.cmd = 0;
   sbuf.mtype = 1;
    return 0;
}

inline int cProtocol_v01 :: shutDownService()
{
// отправляем команду на остановку

int rcv = system("./send_recv 1 5000");

if(rcv == 256)
{
//	printf("сервис завершён корректно\n");
	return 0;
}
else 
{
  printf("сервис завершён c ошибкой %d\n",rcv);
  return -10;
}
}

inline int cProtocol_v01 :: startService(char *adr_to_service)
{
	char command[255];
	int err;

	strcpy(command,"nohup ");
	if(command == 0){

	strcpy(adr_to_service,"./navigator");

	}    
	strncat(command,adr_to_service,strlen(adr_to_service));
	strncat(command," &",2);

//"nohup ../navigator_v1/navigator &"
	err = system(command);
	if(err < 0) return err;
usleep(200000);
/**/
// пингуем сервис
int recv;

recv = system("./send_recv 2 3000");

if(recv == 512){
  printf("сервис ответил на ping\n");
  return 0;}
else {
  printf("сервис получил ответ: %d\n",(int)recv);
  return -10;}
/**/

// ждем, когда пойдёт измерение
  int rcv=0;
  time_start();
  while(rcv != 37376)
  {

	rcv = system("./send_recv 4 300");
	if(time_stop() > 3000000) return -20;
  }
  return 0;
}

inline int cProtocol_v01 :: echo() // ошибки: -1 отправка пакета ; -2 получение пакета ; -3 получен неверный ответ
{

if(system("./send_recv 2 1000") == 512)

{
//	printf("сервис завершён корректно\n");
	return 0;
}
else return -10;
}
/*
int cProtocol_v01 :: protocol_version() // ошибки: -1 отправка пакета ; -2 получение пакета ; -3 получен неверный ответ
{
    sbuf.mtype = 1;
    sbuf.cmd = PROTOCOL_VERSION;
    if(send_msg(omsqid, &sbuf) < 0) return -1;
    usleep(100000);
    if(recv_msg(imsqid, &rbuf) < 0) { return -2;} else
	if(rbuf.cmd == PROTOCOL_VERSION){
		return (int)rbuf.mtext[0] ;
	}else return -3;
}
/**/
inline int cProtocol_v01 :: read_data() // ошибки: -1 блокировка симафора ; -2 открытие временного файла ; -3 чтение из ввременного файла ; -4 разблокировка симафора
{
	return 	read_ind(&data);
}

inline int cProtocol_v01 :: getCoord(DATA_TYPE *Coord,int channel)
{
	int i;
	if(Coord == 0) return -1;

	DATA_TYPE p[6];
	if(data.error_flag != 0) return -1;
	getP(p,&data,channel); 
	for(i=0;i<3;i++) Coord[i] = p[i];

	return 0;
}

inline int cProtocol_v01 :: getOrientation(DATA_TYPE *Orientation,int channel)
{
	int i;
	if(Orientation == 0) return -1;

	DATA_TYPE p[6];
	if(data.error_flag != 0) return -1;
	getP(p,&data,channel); 
	for(i=0;i<3;i++) Orientation[i] = p[i+3];

	return 0;
}

inline unsigned char cProtocol_v01 :: getByteChannel(int channel)//информация о каналах
{
	return data.byte_channels[channel];
}

inline int *cProtocol_v01 :: getChennelsStatus()
{
	return data.channels;
}

inline TIME_TYPE cProtocol_v01 :: getTime()
{
	return data.time;
}

inline int cProtocol_v01 :: getPackNumber()
{
	return data.pack_number;
}

inline int cProtocol_v01 :: getErrorFlag()
{
	return data.error_flag;
}

#endif // NAVIGATOR_INTERFACE_H
